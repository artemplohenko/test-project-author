$(document).ready(function(){

  $('.bxslider').bxSlider({
    pager: false,
    startSlide: 0,
    mode: 'horizontal',
    infiniteLoop: true,
    captions: true,
    nextText: '',
    prevText: '',
    useCSS: true,
    easing: 'jswing',
    speed: 500,
    infiniteLoop: true,
    hideControlOnEnd: true,
    easing: 'easeOutElastic',
  });



  var btns = $('.price-btn .button')
  btns.on('mouseenter', function ( e ) {
    e.target.closest('.price-text').style.backgroundColor = '#3eb3b5';
  });
  btns.on('mouseleave', function ( e ) {
    e.target.closest('.price-text').style.backgroundColor = '';
  });



  $('.toggle-menu').click(function () {
    $('.menu-container').toggleClass('menu-open');
    $(this).toggleClass('active');
  });



  $(".menu").on("click","a", function (event) {
    event.preventDefault();
    var id  = $(this).attr('href'),
    top = $(id).offset().top;
    $('body,html').animate({scrollTop: top}, 1500);
  });


});